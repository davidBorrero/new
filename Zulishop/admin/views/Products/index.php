<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Productos de Zulishop</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Brand</th>
                  <th>Disccount</th>
                </tr>
                <?php foreach ($this->products as $product) : ?>
                <tr>
                  <td><?php print $product["id"]; ?></td>
                  <td><?php print $product["name"]; ?></td>
                  <td><?php print $product["price"]; ?></td>
                  <td><?php print $product["quantity"]; ?></td>
                  <td><?php print $product["brand"]; ?></td>
                  <td><?php print $product["disccount"]; ?></td>
                </tr>
                <?php endforeach; ?>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>